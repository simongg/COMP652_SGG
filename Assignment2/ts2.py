import numpy as np
import matplotlib.pyplot as plt

class TS_Poisson:
        def __init__(self,bandit_arms,alpha0, beta0):
                self.n_arms = bandit_arms
                self.a = alpha0
                self.b = beta0
                self.num_rewards = np.zeros(self.n_arms)
                self.num_trials = np.zeros(self.n_arms) 
        def select(self):
                choice = np.argmax(np.random.gamma(self.num_rewards+self.a,scale=self.b/(self.num_trials*self.b+1)))
                return choice
        def update(self,k_t,r_t):
                self.num_rewards[k_t] += r_t
                self.num_trials[k_t] += 1

means = np.array([1,2,3,4,5])
regrets = np.max(means) - means
K = len(means)
alg = TS_Poisson(K,1,1)
cumul_regrets = [0]

for t in range(10000):
    k_t = alg.select()
    r_t = np.random.poisson(lam=means[k_t])
	#print r_t
    alg.update(k_t, r_t)
#    print(k_t)
    cumul_regrets.append(cumul_regrets[-1]+regrets[k_t])	
plt.plot(cumul_regrets)