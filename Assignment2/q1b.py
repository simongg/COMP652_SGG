#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  5 14:33:11 2018

@author: simon
COMP 652 Assignment 2, Q1b
GP with different kernels, added noise
"""

def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure

from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, DotProduct,\
 RationalQuadratic, WhiteKernel

###############################################################################
# Initial Setup 
###############################################################################
np.random.seed(7)

nsamples = 50
# Get the testing data
xi = np.linspace(0, 10, nsamples)
yi = xi * np.sin( xi )

noise = np.random.normal(0,0.5,nsamples)

yn = yi + noise

# Get the true data
x = np.linspace(0,15, 500)
y = x * np.sin( x )
log_llh = np.zeros((2,5))
###############################################################################
# Q1b_i 
###############################################################################
if 1:
    kernels = [1.1 * RBF(length_scale=0.1, length_scale_bounds=(1e-3, 1.0)),\
               1.1 * RBF(length_scale=10, length_scale_bounds=(1e-3, 10)),\
               DotProduct(sigma_0=10),\
               1.1 * RationalQuadratic(length_scale=0.1, length_scale_bounds=(1e-3, 10)),\
               RationalQuadratic(length_scale=0.1, length_scale_bounds=(1e-3, 1.0))+\
        1.1*RBF(length_scale=0.1, length_scale_bounds=(1e-3, 1.0))]   
    filenames = ['Q1bi_rbf','Q1bi_rbf_ls10','Q1bi_dotprod','Q1bi_ratquad','Q1bi_ratquad_rbf']
    titles = ['RBF Kernel, starting length scale = 0.1', 'RBF Kernel, starting length scale = 10',\
              'DotProduct Kernel','Rational Quadratic Kernel',\
              'sum of Rational Quadratic and RBF Kernels']
        # Get the required kernel and fit it
    for idx, kernel in enumerate(kernels): 
        gp = GaussianProcessRegressor(kernel=kernel)
        gp.fit(xi.reshape(-1,1),yn)
        X = np.linspace(0, 15, 100)
        y_mean, y_std = gp.predict(X.reshape(-1,1), return_std=True)
        params = gp.get_params(True)
        
        log_llh[0][idx] = gp.log_marginal_likelihood()
        
        # Plot RBF Kernel fitted to x*sin(x)
        figure(num=None, figsize=(10, 5), dpi=80, facecolor='w', edgecolor='k')
        plt.plot(x, y)
        plt.scatter(xi, yn)
        plt.plot(X, y_mean, 'k', lw=3, zorder=9)
        plt.fill_between(X, y_mean - y_std**2/2, y_mean + y_std**2/2, alpha=0.2, color='k')
        
        plt.title(titles[idx] + ' with added noise \n log_marginal_likelihood = {}'\
                  .format(gp.log_marginal_likelihood()))
        plt.legend(['True Value','Y predicted','Sampled Data','Y Prediction Accuracy'])
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.savefig(filenames[idx]+'.jpg',  format="jpg")
        plt.show()

###############################################################################
# Q1b_ii
###############################################################################
if 1:
    kernels = [1.1 * RBF(length_scale=0.1, length_scale_bounds=(1e-3, 1.0)),\
               1.1 * RBF(length_scale=10, length_scale_bounds=(1e-3, 10)),\
               DotProduct(sigma_0=10),\
               RationalQuadratic(length_scale=.10,length_scale_bounds=(1e-3, 1.0)),\
               RationalQuadratic(length_scale=0.1, length_scale_bounds=(1e-3, 1.0))+\
        1.1*RBF(length_scale=0.1, length_scale_bounds=(1e-3, 1.0))]   
    filenames = ['Q1bii_rbf','Q1bii_rbf_ls10','Q1bii_dotprod','Q1bii_ratquad',\
                 'Q1bii_ratquad_rbf']
    titles = ['RBF Kernel, starting length scale = 0.1', 'RBF Kernel, starting length scale = 10',\
              'DotProduct Kernel','Rational Quadratic Kernel',\
              'sum of Rational Quadratic and RBF Kernels']

        # Get the required kernel and fit it
    for idx, kernel in enumerate(kernels): 
        gp = GaussianProcessRegressor(kernel=kernel + WhiteKernel(noise_level=0.5))
        gp.fit(xi.reshape(-1,1),yn)
        X = np.linspace(0, 15, 100)
        y_mean, y_std = gp.predict(X.reshape(-1,1), return_std=True)
        params = gp.get_params(True)

        log_llh[1][idx] = gp.log_marginal_likelihood()

        # Plot RBF Kernel fitted to x*sin(x)
        # figsize(10,5)
        # For laptop figsize(20,15)
        figure(num=None, figsize=(10, 5), dpi=80, facecolor='w', edgecolor='k')
        plt.plot(x, y)
        plt.scatter(xi, yn)
        plt.plot(X, y_mean, 'k', lw=3, zorder=9)
        plt.fill_between(X, y_mean - y_std**2/2, y_mean + y_std**2/2, alpha=0.2, color='k')
        
        plt.title(titles[idx] + ' with WhiteGaussian Kernel \n log_marginal_likelihood = {}'\
                  .format(gp.log_marginal_likelihood()))
        plt.legend(['True Plot','Predicted Y (mean)','Sampled Data','Variance of Y'])
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.savefig(filenames[idx]+'.jpg',  format="jpg")
        plt.show()