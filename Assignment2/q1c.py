#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  5 16:08:51 2018

@author: simon
COMP 652 Assignment 2, Q1c
GP with different kernels, added noise, added white Kernel
"""
def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
from sklearn.preprocessing import StandardScaler

from sklearn.gaussian_process import GaussianProcessRegressor
import operator

from sklearn.gaussian_process.kernels import RBF, WhiteKernel,\
 RationalQuadratic, ExpSineSquared
 
fig_width = 7.5
fig_height = 5

# Set up scaling function and set random seed
scaler_x = StandardScaler()
scaler_y = StandardScaler()
np.random.seed(7)

# Read and separate data
df = pd.read_csv('mauna.csv')

xtrn = df['trainyear'].values.reshape(-1,1)
ytrn = df['trainCO2'].values

xtst = df['testyear'].values.reshape(-1,1)
ytst = df['testCO2'].values

xtst = xtst[~np.isnan(xtst)].reshape(-1,1)
ytst = ytst[~np.isnan(ytst)].reshape(-1,1)

# Scale data
xtrn_scld = scaler_x.fit_transform(xtrn)
ytrn_scld = scaler_y.fit_transform(ytrn.reshape(-1,1))
xtst_scld = scaler_x.transform(xtst)
ytst_scld = scaler_y.transform(ytst.reshape(-1,1))

figure(num=None, figsize=(fig_width, fig_height), dpi=80, facecolor='w', edgecolor='k')
plt.plot(xtrn_scld, ytrn_scld)
plt.scatter(xtst_scld, ytst_scld)
plt.title('Scaled Data')
plt.xlabel('Year')
plt.ylabel('CO2')
plt.show()

# A really good setup I found, but this one has positive log-marginal-likelihood
#    RBFAmp = 29.76
#    LTS = 29.76
#    periodAmp = 11.29
#    pdls = 30.39
#    period_ls = 18.87
#    periodicity = 0.07565 # ~= 3.1783/47
#    ratQuadAmplitude = 0.161
#    ratQuad_ls = 1.374
#    mr_RBFAmp = 0.0189
#    mr_RBF_ls = 0.3290
#    nl = 0.001
#    wkAmp = 1

###############################################################################
# Get kernel
###############################################################################
numscale = 1 # Set to 1 to see the plot, set to other values to get a 
# log-marginal-likelihood plot
scale = np.linspace(0.0001,0.1, numscale)
lml = []
# Sweep through a range to get the best value
for ii in scale:
    
    RBFAmp = 1
    LTS = 20
    periodAmp = 20
    pdls = 50
    period_ls = 5
    period = 0.0758687 # 0.0757 # ~= 3.1783/47
    ratQuadAmplitude = 1.9
    ratQuad_ls = 1
    mr_RBFAmp = 0.01
    mr_RBF_ls = 1
    nl = 0.0001
    wkAmp = 1
    
    kernel = RBFAmp**2 * RBF(length_scale=LTS)\
    + WhiteKernel(noise_level=nl)\
    + periodAmp**2 * RBF(length_scale=pdls)\
    * ExpSineSquared(length_scale=period_ls, periodicity=period)\
    + mr_RBFAmp**2 * RBF(length_scale=mr_RBF_ls)\
    + ratQuadAmplitude**2 * RationalQuadratic(alpha=20, length_scale=ratQuad_ls)\
    
    gp = GaussianProcessRegressor(kernel=kernel, optimizer=None,\
                                  normalize_y=False)
    gp.fit(xtrn_scld, ytrn_scld)
    lml.append(gp.log_marginal_likelihood())

# Get minimum log likelihood and print value
max_lml_idx, max_lml = max(enumerate(lml), key=operator.itemgetter(1))
print(scale[max_lml_idx])

###############################################################################
# Print either log-marginal-likelihood or fitted data
###############################################################################

if numscale != 1: # if numscale isnt 1, plot the log-marginal-likelihood
    figure(num=None, figsize=(fig_width, fig_height), dpi=80, facecolor='w', edgecolor='k')
    plt.semilogx(scale, lml)
    plt.title('Max lml: {}, length_scale = {}'.format(max_lml, scale[max_lml_idx]))
    plt.show()

if numscale == 1: # If numscale = 1, plot fitting graph
    # Create a curve with the fitted data
    X_ = np.linspace(min(xtrn_scld), max(xtst_scld)+.5, 5000)
    y_mean, y_std = gp.predict(X_.reshape(-1,1), return_std=True)
    
    # Plot RBF Kernel fitted to the mauna data
    figure(num=None, figsize=(fig_width, fig_height), dpi=80, facecolor='w', edgecolor='k')
    plt.plot(xtrn_scld, ytrn_scld)
    plt.scatter(xtst_scld, ytst_scld)
    plt.plot(X_, y_mean, 'k', lw=3, zorder=9)
    plt.fill_between(X_, y_mean[:,0] - y_std, y_mean[:,0] + y_std, alpha=0.2, color='k')
    plt.title('GPR\nLML = {}'.format(gp.log_marginal_likelihood()))
    plt.xlabel('Year')
    plt.ylabel('$CO_2$ Concentration')
    plt.show()
    
    # Create a curve with the test data
    X_ = np.linspace(min(xtst_scld), max(xtst_scld) + .5, 1000)
    y_mean, y_std = gp.predict(X_.reshape(-1,1), return_std=True)
    
	# Plot RBF Kernel fitted to the mauna test data
    figure(num=None, figsize=(fig_width, fig_height), dpi=80, facecolor='w', edgecolor='k')
    plt.scatter(xtst_scld, ytst_scld)
    plt.plot(X_, y_mean, 'k', lw=3, zorder=9)
    plt.fill_between(X_, y_mean[:,0] - y_std**2, y_mean[:,0] + y_std**2, alpha=0.2, color='k')
    plt.title('Fitted GPR with Test Data\nLML = {}\nKernel: '.format(gp.log_marginal_likelihood(), kernel))
    plt.xlabel('Year')
    plt.ylabel('$CO_2$ Concentration')
    plt.show()