# -*- coding: utf-8 -*-
"""
Created on Thu Oct 18 20:29:25 2018
@author: Upper Confidence Bounds on a 2-armed Bernoulli bandit
"""
# Implementing UCB
import numpy as np
from numpy.random import binomial
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure

class UCB_Bern:
        def __init__(self,bandits,delta):
                self.arms = bandits
                self.delta = delta
                self.rewards = np.zeros(self.arms)
                self.trials = np.ones(self.arms) 
        def update(self,k_t,r_t):
                self.rewards[k_t] += r_t
                self.trials[k_t] += 1     
        def select(self):
                choice = np.argmax(self.rewards/self.trials + np.sqrt(2*np.log(1/self.delta)/self.trials))
                return choice
        def select_initial(self, mu_t):
#                print(self.rewards/self.trials )
                choice = np.argmax(self.rewards/self.trials + np.sqrt(2*np.log(1/self.delta)/self.trials))
                return choice

means = np.array([0.2, 0.6])
regrets = np.max(means) - means
K = len(means)
deltas = np.logspace(-1, -1, 1)
iterations = 1000
repetitions = 50
total_regret = []
regret_ratio = []

for delta in deltas:
        
    for _ in range(repetitions):
        regret_sum = [0]
    
        alg = UCB_Bern(K,delta)
        mu_t = [binomial(n=1, p=means[0]), binomial(n=1, p=means[1])]
        trys_init = 0;
        while not(max(mu_t)): # if all 0s, will not work. Retry
            mu_t = [binomial(n=1, p=means[0]), binomial(n=1, p=means[1])]
            trys_init += 1 # Add a value to the trys for both bandits
            
        alg.trials = np.array([x+trys_init for x in alg.trials]) 
        alg.rewards = np.array([x+mu_t[idx] for idx, x in enumerate(alg.rewards)])
        
        k_t = alg.select_initial(mu_t)
        for _ in range(iterations):
            r_t = binomial(n=1, p=means[k_t])
            alg.update(k_t, r_t)
            regret_sum.append(regret_sum[-1]+regrets[k_t])	
            k_t = alg.select()
        total_regret.append(regret_sum)
    
    min_idx = np.argmin([item[-1] for item in total_regret])
    max_idx = np.argmax([item[-1] for item in total_regret])
    mean_regret_sum = np.mean(total_regret, axis=0)
    regret_ratio.append(total_regret[max_idx][-1]/total_regret[min_idx][-1])
    figure(num=None, figsize=(10, 5), dpi=80, facecolor='w', edgecolor='k')
    plt.plot(mean_regret_sum)
    plt.plot(total_regret[min_idx])
    plt.plot(total_regret[max_idx])
    plt.title('Average Cumulative Regret over 50 repetitions, delta = {}\n mean1 = {}, mean2 = {}'.format(delta,\
              means[0],means[1]))
    plt.xlabel('Iterations')
    plt.ylabel('Regret')
    plt.legend(['Average Cumulative Regret', 'Minimum Cumulative Regret','Maximum Cumulative Regret'])
    plt.savefig('UCB_Bern_delta{}_p{}_p{}.jpg'.format(delta,means[0],means[1]),  format="jpg")

#figure(num=None, figsize=(10, 5), dpi=80, facecolor='w', edgecolor='k')
#plt.semilogx(deltas, regret_ratio)
#plt.title('Ratio of the max regret and min regret')
#plt.xlabel('Delta')
#plt.ylabel('Regret Ratio')
#plt.legend(['Regret Ratio'])
#plt.savefig('Figures/UCB_Bern_regret_ratio.jpg',  format="jpg")
