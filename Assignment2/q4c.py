# -*- coding: utf-8 -*-
"""
Created on Thu Oct 18 17:52:55 2018
@author: Simon
Thompson Sampling with Poisson Distribution
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
from numpy.random import poisson, gamma

class TS_poisson:
        def __init__(self, bandit_arms, alpha0, beta0):
                self.n_arms = bandit_arms
                self.rewards = np.zeros(self.n_arms)
                self.trials = np.zeros(self.n_arms) 
                self.a = alpha0
                self.b = beta0

        def update(self, k_t, r_t):
                self.rewards[k_t] += r_t
                self.trials[k_t] += 1
                
        def select(self):
                selected = np.argmax(gamma(self.rewards + self.a, scale = self.b/(self.trials*self.b + 1)))
                return selected

np.random.seed(3)
repetitions = 20
iterations = 10000
total_regret = []
for _ in range(repetitions):
    regret_sum = [0]
    means = np.array([1,2,3,4,5])
    regrets = np.max(means) - means
    K = len(means)
    alg = TS_poisson(K,1,1)

    for _ in range(iterations):
        k_t = alg.select()
        r_t = poisson(lam=means[k_t])
        alg.update(k_t, r_t)
        regret_sum.append(regret_sum[-1]+regrets[k_t])
        
    # Append current regret sum to total regret
    total_regret.append(regret_sum)  
    
min_idx = np.argmin([item[-1] for item in total_regret])
max_idx = np.argmax([item[-1] for item in total_regret])
mean_total_regret = np.mean(total_regret, axis=0)

figure(num=None, figsize=(10, 5), dpi=80, facecolor='w', edgecolor='k')
plt.plot(mean_total_regret)
plt.plot(total_regret[min_idx])
plt.plot(total_regret[max_idx])
plt.title('Cumulative Regret')
plt.xlabel('Iterations')
plt.ylabel('Regret')
plt.legend(['Average Cumulative Regret', 'Minimum Cumulative Regret','Maximum Cumulative Regret'])
plt.savefig('TS_poisson.jpg',  format="jpg")
