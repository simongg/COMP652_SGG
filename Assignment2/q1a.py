#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  5 11:57:17 2018

@author: simon
COMP 652 Assignment 2, Q1a
GP with different kernels, no noise
"""
def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure

from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, DotProduct, RationalQuadratic

###############################################################################
# Initial Setup 
###############################################################################
nsample = 50
# Get the testing data
xi = np.linspace(0, 10, nsample)
yi = xi * np.sin( xi )

# Get the true data
x = np.linspace(0,15, 500)
y = x * np.sin( x )
    
###############################################################################
# Q1a
###############################################################################
# Define all the kernels
kernels = [1.1 * RBF(length_scale=0.5, length_scale_bounds=(1e-3, 1.0)),\
           1.1 * RBF(length_scale=10, length_scale_bounds=(1e-3, 15)),\
           1.1 * DotProduct(sigma_0=1),\
           1.1 * RationalQuadratic(length_scale=0.1, length_scale_bounds=(1e-3, 1)),\
           1.1 * RationalQuadratic(length_scale=1, length_scale_bounds=(1e-3, 1.0))\
   + 1.1*RBF(length_scale=1, length_scale_bounds=(1e-3, 1.0))]  
# kernels = [1.1 * RationalQuadratic(length_scale=0.1,
# length_scale_bounds=(1e-3, 1.0)) + 1.1*RBF(length_scale=0.1, length_scale_bounds=(1e-3, 1.0))] 
filenames = ['Q1ai_rbf','Q1ai_rbf_ls10','Q1aii_dotprod','Q1aiii_ratquad','Q1aiv_ratquad_rbf']
titles = ['RBF Kernel, starting length scale = 0.1', 'RBF Kernel, starting length scale = 10',\
              'DotProduct Kernel','Rational Quadratic Kernel',\
              'sum of Rational Quadratic and RBF Kernels']

# Get the required kernel and fit it
for idx, kernel in enumerate(kernels): 
    gp = GaussianProcessRegressor(kernel=kernel)
    gp.fit(xi.reshape(-1,1),yi)
    X = np.linspace(0, 15, 100)
    y_mean, y_std = gp.predict(X.reshape(-1,1), return_std=True)
    params = gp.get_params(True)

    # Plot RBF Kernel fitted to x*sin(x)
    # For surface figsize(10,5)
    figure(num=None, figsize=(10, 5), dpi=80, facecolor='w', edgecolor='k')
    plt.plot(x, y)
    plt.scatter(xi, yi)
    plt.plot(X, y_mean, 'k', lw=3, zorder=9)
    plt.fill_between(X, y_mean - y_std**2, y_mean + y_std**2, alpha=0.2, color='k')
    
    plt.title(titles[idx] + '\n log_marginal_likelihood = {}'\
                  .format(gp.log_marginal_likelihood()))
    plt.legend(['True Plot','Predicted Y (mean)','Sampled Data','Variance of Y'])
    plt.xlabel('X')
    plt.ylabel('Y')
    plt.savefig(filenames[idx]+'.jpg',  format="jpg")
    plt.show()
