# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 17:08:24 2018

@author: Simon
"""
import numpy as np

def q1e(X, sigma=0.1, mus=[np.linspace(-1, 1, 5),np.linspace(-1, 1, 5)]):
    xbasis = []
    for mu in np.array(mus).T:
        for idx, xcol in enumerate(X.T):
#            xcol -= mu
            xbasis.append(np.exp(-(xcol - mu[idx]) ** 2/(2 * sigma)).tolist())    
    return np.array(xbasis).T
