# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 17:09:14 2018

@author: Simon
"""
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold 
from sklearn import preprocessing
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
import operator

def ridge_regression(data, power, models_to_plot, lamb):
        #initialize predictors:
    predictors=['x']
    if power>=2:
        predictors.extend(['x_%d'%i for i in range(2,power+1)])
    #Fit the model
    linreg = Ridge(normalize=False, alpha = lamb, fit_intercept  = False)
    
    linreg.fit(data[predictors],data['y'])
    prediction = linreg.predict(data[predictors])
    w = []
    #Return the result in pre-defined format
    err = sum((prediction - data['y'])**2) + sum(linreg.coef_**2)

    w.append(linreg.intercept_)
    w.extend(linreg.coef_)
                
    return err, w

def lasso_regression(data, power, models_to_plot, lamb, max_iter=1e10):
        #initialize predictors:
    predictors=['x']
    if power>=2:
        predictors.extend(['x_%d'%i for i in range(2,power+1)])
    #Fit the model
    lassoreg = Lasso(normalize = False, alpha = lamb, fit_intercept = False)
    
    lassoreg.fit(data[predictors],data['y'])
    prediction = lassoreg.predict(data[predictors])
    w = []
    #Return the result in pre-defined format
    err = sum((prediction - data['y'])**2) + sum(abs(lassoreg.coef_))

    w.extend(lassoreg.coef_)
                
    return err, w

def linear_regression(data, power, models_to_plot):
    
    #initialize predictors:
    predictors=['x']
    if power>=2:
        predictors.extend(['x_%d'%i for i in range(2,power+1)])
    #Fit the model
    linreg = LinearRegression(normalize=True)
    linreg.fit(data[predictors],data['y'])
    prediction = linreg.predict(data[predictors])
    w = []
    #Return the result in pre-defined format
    err = sum((prediction - data['y'])**2)

    w.append(linreg.intercept_)
    w.extend(linreg.coef_)
                
    return err, w
            
def q1i_dim(plotting, lamb = 0):
#    np.random.seed(7)
    w_star = []
    w_star_norm = []
    Jtrain = []
    Jvalid = []
    Jtest = []
    dims = [1,2,3,5,9]
    
    # Load Data
    Y = np.loadtxt('hw1-q1y.csv')
    # standardize y dataset
    Y = (Y - np.mean(Y)) / np.std(Y)
    
    X = np.loadtxt('hw1-q1x.csv')[:,1]
    
    # Normalize the x variable
    X = (X - np.mean(X)) / np.std(X)     

    for power in dims:
        
        # Split data
        x, xt, y, yt = train_test_split(X, Y, test_size=0.2)
        x = preprocessing.scale(x)
        xt = preprocessing.scale(xt)
        y = preprocessing.scale(y)
        yt = preprocessing.scale(yt)
        
        data = pd.DataFrame(np.column_stack([x,y]),columns=['x','y'])
        test_data = pd.DataFrame(np.column_stack([xt,yt]),columns=['xt','yt'])
        
        
        for i in range(2,dims[-1]+1):  #power of 1 is already there
            colname = 'x_%d'%i      #new var will be x_power
            data[colname] = preprocessing.scale(data['x']**i)
#        print(data.head())
        
        predictors=['x']
        if power>=2:
            predictors.extend(['x_%d'%i for i in range(2,power+1)])
            
        tst_predictors=['xt']
        if power>=2:
            tst_predictors.extend(['xt_%d'%i for i in range(2,power+1)])
            
        #Define the powers for which a plot is required:
        models_to_plot = {1:411,3:412,5:413,9:414}
        
        for i in range(2,dims[-1]+1):  #power of 1 is already there, get the max
            colname = 'x_%d'%i      #new var will be x_power
            colnametst = 'xt_%d'%i  # New tst var will be xtst_power
            data[colname] = data['x']**i
            test_data[colnametst] = test_data['xt']**i
            
#        print(data.head())
                
        kf = KFold(n_splits=5, random_state=None, shuffle=True) # Define the split - into 5 folds 
        kf.get_n_splits(data)
        
        #initialize predictors:
        predictors=['x']
        if power>=2:
            predictors.extend(['x_%d'%i for i in range(2,power+1)])
            
        w_star_curr = []
        w = 0
        Jtrn = []
        Jval = []
        
        for train_idx, valid_idx in kf.split(data):
            loss, w = linear_regression(data.iloc[train_idx], power, models_to_plot)
            prediction_trn = data[predictors].iloc[train_idx].dot(w[1:]) + w[0]
            prediction_val = data[predictors].iloc[valid_idx].dot(w[1:]) + w[0]
            
            Jtrn.append(sum((prediction_trn - data['y'].iloc[train_idx])**2))
            Jval.append(sum((prediction_val - data['y'].iloc[valid_idx])**2))
            
            w_star_curr.append(w)
           
        min_j_idx, min_j = min(enumerate(Jval), key=operator.itemgetter(1))
        w_star.append(np.array(w_star_curr[min_j_idx]))
        w_star_norm.append(np.linalg.norm(w_star[-1]))  
        Jtrain.append(np.mean(Jtrn)/len(x[train_idx]))
        Jvalid.append(np.mean(Jval)/len(x[valid_idx]))
        prediction_tst = test_data[tst_predictors].dot(w_star[-1][1:]) + w_star[-1][0]
        Jtest.append(sum((prediction_tst - test_data['yt'] )**2/len(xt)))
        
        
    if plotting:
        plt.plot(dims, w_star_norm)
        plt.xlabel('Dimension')
        plt.ylabel('Norm of Weights')
        plt.title("Norm of Weights")
        plt.savefig('Figures/CVNormWeights_Q1i.jpg',  format="jpg")
        plt.show()
        
        for pwIdx, power in enumerate([1,2,3,5,9]):
            xtst = np.linspace(min(X)*1.05, max(X)*1.05, 100)
            y_act_pred = np.zeros(len(xtst))
            for ii in range(power+1):
#                print(ii)
                y_act_pred += w_star[pwIdx][ii] * np.power(xtst, ii)
            plt.plot(xtst, y_act_pred)
      
        plt.scatter(data['x'], data['y'])
        plt.scatter(test_data['xt'], test_data['yt'])        
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.title("Fittings")
        plt.legend(['Y1','Y2','Y3','Y5','Y9','Scatter Train', 'Scatter Test'])
        plt.savefig('Figures/Fittings_Q1i.jpg',  format="jpg")
        plt.show()

        plt.clf()
        plt.plot(dims, Jtest)
        plt.plot(dims, Jvalid)
        plt.plot(dims, Jtrain)
        plt.xlabel('Dimension')
        plt.ylabel('Cost')
        plt.legend(['J Test','J Validation', 'J Train'])
        plt.title("Cost Function for Various Orders")
        plt.savefig('Figures/CostFcn_Q1i.jpg',  format="jpg")
        plt.show()
        
         
def q1i_l2(plotting, lambs = [0.01, 0.1, 1, 10]):
#    np.random.seed(7)
    w_star = []
    w_star_norm = []
    Jtrain = []
    Jvalid = []
    Jtest = []
    dims = [9]
    
    # Load Data
    Y = np.loadtxt('hw1-q1y.csv')
    # standardize y dataset
    Y = preprocessing.scale(Y)
    
    X = np.loadtxt('hw1-q1x.csv')[:,1]
    X = preprocessing.scale(X)
      
    power = 9
    
    for lamb in lambs:
        
        # Split data
        x, xt, y, yt = train_test_split(X, Y, test_size=0.2)
#        x = preprocessing.scale(x)
#        xt = preprocessing.scale(xt)
#        y = preprocessing.scale(y)
#        yt = preprocessing.scale(yt)
        
        data = pd.DataFrame(np.column_stack([x,y]),columns=['x','y'])
        
        test_data = pd.DataFrame(np.column_stack([xt,yt]),columns=['xt','yt'])
        
        
        for i in range(2,dims[-1]+1):  #power of 1 is already there
            colname = 'x_%d'%i      #new var will be x_power
            data[colname] = (data['x']**i)
#        print(data.head())
        
        
        predictors=['x']
        if power>=2:
            predictors.extend(['x_%d'%i for i in range(2,power+1)])
            
        tst_predictors=['xt']
        if power>=2:
            tst_predictors.extend(['xt_%d'%i for i in range(2,power+1)])
            
        #Define the powers for which a plot is required:
        models_to_plot = {0.01:411,0.1:412,1:413,10:414}
        
        for i in range(2,dims[-1]+1):  #power of 1 is already there, get the max
            colname = 'x_%d'%i      #new var will be x_power
            colnametst = 'xt_%d'%i  # New tst var will be xtst_power
            data[colname] = (data['x']**i)
            test_data[colnametst] = (test_data['xt']**i)
            
#        print(data.head())
                
        kf = KFold(n_splits=5, random_state=None, shuffle=True) # Define the split - into 5 folds 
        kf.get_n_splits(data)
        
        #initialize predictors:
        predictors=['x']
        if power>=2:
            predictors.extend(['x_%d'%i for i in range(2,power+1)])
            
        w_star_curr = []
        w = 0
        Jtrn = []
        Jval = []
        
        for train_idx, valid_idx in kf.split(data):
            loss, w = ridge_regression(data.iloc[train_idx], power, models_to_plot, lamb)
            w = np.array(w)
            prediction_trn = data[predictors].iloc[train_idx].dot(w[1:]) + w[0]
            prediction_val = data[predictors].iloc[valid_idx].dot(w[1:]) + w[0]
            
            Jtrn.append(sum((prediction_trn - data['y'].iloc[train_idx])**2) + lamb*(np.matmul(w.T,w)))
            Jval.append(sum((prediction_val - data['y'].iloc[valid_idx])**2) + lamb*(np.matmul(w.T,w)))
            
            w_star_curr.append(w)
#        print(lamb)
        min_j_idx, min_j = min(enumerate(Jval), key=operator.itemgetter(1))
        w_star.append(np.array(w_star_curr[min_j_idx]))
        w_star_norm.append(np.linalg.norm(w_star[-1]))  
        Jtrain.append(np.mean(Jtrn)/len(x[train_idx]))
        Jvalid.append(np.mean(Jval)/len(x[valid_idx]))
        prediction_tst = test_data[tst_predictors].dot(w_star[-1][1:]) + w_star[-1][0]
        Jtest.append(sum((prediction_tst - test_data['yt'] )**2/len(xt)) + lamb*(np.matmul(w.T,w)))
        
        
    if plotting:
        
        plt.semilogx(lambs, w_star)
        plt.xlabel('Lambda')
        plt.ylabel('Weights')
        plt.title("Weights for Various Lambdas, L2 regularization")
        plt.savefig('Figures/CVWeights_Q1i_l2.jpg',  format="jpg")
        plt.show()
        
        plt.semilogx(lambs, w_star_norm)
        plt.xlabel('Lambda')
        plt.ylabel('Norm of Weights')
        plt.title("Norm of Weights for Various Lambdas, L2 regularization")
        plt.savefig('Figures/CVNormWeights_Q1i_l2.jpg',  format="jpg")
        plt.show()
        
        for lambIdx, lamb in enumerate(lambs):
            for pwIdx, power in enumerate([9]):
                xtst = np.linspace(min(X)*1.05, max(X)*1.05, 100)
                y_act_pred = np.zeros(len(xtst))
                for ii in range(power+1):
    #                print(ii)
                    y_act_pred += w_star[lambIdx][ii] * np.power(xtst, ii)
                plt.plot(xtst, y_act_pred)
        print(w_star[:][0])

        plt.scatter(data['x'], data['y'])
        plt.scatter(test_data['xt'], test_data['yt'])        
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.title("Fittings, L2 regularization")
        plt.legend(['lambda = 0.01','lambda = 0.1','lambda = 1','lambda = 10','Scatter Train', 'Scatter Test'])
        plt.savefig('Figures/Fittings_Q1i_l2.jpg',  format="jpg")
        plt.show()
        
        plt.clf()
        plt.semilogx(lambs, Jtest)
        plt.semilogx(lambs, Jvalid)
        plt.semilogx(lambs, Jtrain)
        plt.xlabel('Lambda')
        plt.ylabel('Cost')
        plt.legend(['J Test','J Validation', 'J Train'])
        plt.title("Cost Function for Various Lambdas, L2 regularization")
        plt.savefig('Figures/CostFcn_Q1i_l2.jpg',  format="jpg")
        plt.show()
        
def q1i_l1(plotting, lambs = [0.01, 0.1, 1, 10]):
#    np.random.seed(7)
    w_star = []
    w_star_norm = []
    Jtrain = []
    Jvalid = []
    Jtest = []
    dims = [9]
#    lambs = np.logspace(-2,1,20)
    
    # Load Data
    Y = np.loadtxt('hw1-q1y.csv')
    # standardize y dataset
#    Y = preprocessing.scale(Y)
    
    X = np.loadtxt('hw1-q1x.csv')[:,1]
    
    # Normalize the x variable
#    X = preprocessing.scale(X)  
    power = 9
    
    for lamb in lambs:
        
        # Split data
        x, xt, y, yt = train_test_split(X, Y, test_size=0.2)

        
        data = pd.DataFrame(np.column_stack([x,y]),columns=['x','y'])
        
        test_data = pd.DataFrame(np.column_stack([xt,yt]),columns=['xt','yt'])
        
        predictors=['x']
        if power>=2:
            predictors.extend(['x_%d'%i for i in range(2,power+1)])
            
        tst_predictors=['xt']
        if power>=2:
            tst_predictors.extend(['xt_%d'%i for i in range(2,power+1)])
            
        #Define the powers for which a plot is required:
        models_to_plot = {0.01:411,0.1:412,1:413,10:414}
        
        data['x'] = preprocessing.scale(data['x'])
        data['y'] = preprocessing.scale(data['y'])
        test_data['xt'] = preprocessing.scale(test_data['xt'])
        test_data['yt'] = preprocessing.scale(test_data['yt'])
        for i in range(2,dims[-1]+1):  #power of 1 is already there, get the m
            colname = 'x_%d'%i      #new var will be x_power
            colnametst = 'xt_%d'%i  # New tst var will be xtst_power
            data[colname] = (data['x']**i)
            test_data[colnametst] = (test_data['xt']**i)
#            data[colname] = preprocessing.scale(data['x']**i)
#            test_data[colnametst] = preprocessing.scale(test_data['xt']**i)

        
#        print(data.head())
                
        kf = KFold(n_splits=5, random_state=None, shuffle=True) # Define the split - into 5 folds 
        kf.get_n_splits(data)
        
        #initialize predictors:
        predictors=['x']
        if power>=2:
            predictors.extend(['x_%d'%i for i in range(2,power+1)])
            
        w_star_curr = []
        w = 0
        Jtrn = []
        Jval = []
        
        for train_idx, valid_idx in kf.split(data):
            loss, w = lasso_regression(data.iloc[train_idx], power, models_to_plot, lamb)
            w = np.array(w)
            prediction_trn = data[predictors].iloc[train_idx].dot(w[0:])
            prediction_val = data[predictors].iloc[valid_idx].dot(w[0:]) 
            Jtrn.append(sum((prediction_trn - data['y'].iloc[train_idx])**2)/len(x[train_idx]) + lamb*(sum(abs(w))))
            Jval.append(sum((prediction_val - data['y'].iloc[valid_idx])**2)/len(x[valid_idx]) + lamb*(sum(abs(w))))
            w_star_curr.append(w)
            
        min_j_idx, min_j = min(enumerate(Jval), key=operator.itemgetter(1))
        w_star.append(np.array(w_star_curr[min_j_idx]))
        w_star_norm.append(np.linalg.norm(w_star[-1]))  
        Jtrain.append(np.mean(Jtrn))
        Jvalid.append(np.mean(Jval))
        prediction_tst = test_data[tst_predictors].dot(w_star[-1][0:])
        Jtest.append(sum((prediction_tst - test_data['yt'] )**2/len(xt)) + lamb*(sum(abs(w)/len(xt))))
        
        
    if plotting:
        
        plt.semilogx(lambs, w_star)
        plt.xlabel('Lambda')
        plt.ylabel('Weights')
        plt.title("Weights for Various Lambdas, L1 regularization")
        plt.savefig('Figures/CVWeights_Q1i_l1.jpg',  format="jpg")
        plt.show()
        
        plt.semilogx(lambs, w_star_norm)
        plt.xlabel('Lambda')
        plt.ylabel('Norm of Weights')
        plt.title("Norm of Weights for Various Lambdas, L1 regularization")
        plt.savefig('Figures/CVNormWeights_Q1i_l1.jpg',  format="jpg")
        plt.show()
        
        pastData = 1.05
        for lambIdx, lamb in enumerate(lambs):
            for pwIdx, power in enumerate([9]):
                xtst = np.linspace(min(data['x'])*pastData, max(data['x'])*pastData, 100)
                y_act_pred = np.zeros(len(xtst))
                for ii in range(power):
                    y_act_pred += w_star[lambIdx][ii] * np.power(xtst, ii+1)
                plt.plot(xtst, y_act_pred)
        plt.scatter(data['x'], data['y'])
        plt.scatter(test_data['xt'], test_data['yt'])        
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.title("Fittings, L1 regularization")
        plt.legend(['lambda = 0.01','lambda = 0.1','lambda = 1','lambda = 10','Scatter Train', 'Scatter Test'])
        plt.savefig('Figures/Fittings_Q1i_l1.jpg',  format="jpg")
        plt.show()
        
        plt.clf()
        plt.semilogx(lambs, Jtest)
        plt.semilogx(lambs, Jvalid)
        plt.semilogx(lambs, Jtrain)
        plt.xlabel('Lambda')
        plt.ylabel('Cost')
        plt.legend(['J Test','J Validation', 'J Train'])
        plt.title("Cost Function for Various Lambdas, L1 regularization")
        plt.savefig('Figures/CostFcn_Q1i_l1.jpg',  format="jpg")
        plt.show()
        return data
#q1i_dim(True)
#q1i_l2(True)
data = q1i_l1(True)