# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 16:23:10 2018

@author: Simon
"""
from sklearn.model_selection import train_test_split
import numpy as np
import ML_101_SGG as ml
from sklearn import preprocessing
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from q1e import q1e

def q1g(plotting):
#        np.random.seed(7)
        w_gd = []
        w_gd_norm = []
        w_star = []
        w_star_norm = []
        Jtrn = []
        Jtst = []      
        JtstGD = []
        JtrnGD = []
        
        sigmas = [0.1, 0.5, 1, 5]
#        sigmas = np.linspace(0.1, 5, 100)
        # No Lambda
        lmbd = 0
        
        for sigma in sigmas:
            # Load Data
            x = np.loadtxt('hw1-q1x.csv')
            y = np.loadtxt('hw1-q1y.csv')
            
                # Normalize the x variable
            # standardize dataset
            x = preprocessing.scale(x)
            y = preprocessing.scale(y)  
            kmeans = KMeans(n_clusters=5, random_state=0).fit(x)
            means = kmeans.cluster_centers_
            x = q1e(x,sigma, means.T)
            x = preprocessing.scale(x)

#            print(x)
            # Split X and Y data
            x, xt, y, yt = train_test_split(x, y, test_size=0.2)
            B = np.zeros(np.shape(x)[1])
            Y = np.array(y)
#            inital_cost = cost_fcn_l2(x, Y, B)
            
            w_star.append(ml.get_model_L2(x,y,lmbd)) 
            # Training
            newB, cost_history = ml.gradient_descent(x, Y, B, alpha=0.001, iterations=10000, lamb=0, l = False)
            
#            print(inital_cost)
#            print(np.sqrt(cost_history[-1]))
            w_gd.append(newB)
            
            Jtst.append(ml.cost_fcn_l2(xt, yt, w_star[-1], lmbd))
            Jtrn.append(ml.cost_fcn_l2(x, y, w_star[-1], lmbd))
            
            JtstGD.append(ml.cost_fcn_l2(xt, yt, newB, lmbd))
            JtrnGD.append(cost_history[-1])
            
            # Append optimal weights
            w_star_norm.append(np.linalg.norm(w_star[-1]))
            w_gd_norm.append(np.linalg.norm(w_gd[-1]))
        
        if plotting:
            # Minimum cost data from Jq1f
            Jq1f_tst = 0.0845958540461413
            Jq1f_trn = 0.08162466559248664
            
#            plt.plot(sigmas, w_star_norm)
            plt.plot(sigmas, w_gd_norm)
            plt.xlabel('Sigma')
            plt.ylabel('Norm of Weights')
            plt.title("Norm of regression weights")
            plt.legend(["Norm of Weights"])
            plt.savefig('Figures/RegWeightsNorm_Q1g.jpg',  format="jpg")
            plt.show()
            
#            plt.plot(sigmas, w_star)
            plt.plot(sigmas, w_gd)
            plt.xlabel('Sigma')
            plt.ylabel('Weights')
            plt.title("Weights")
            plt.savefig('Figures/RegWeights_Q1g.jpg',  format="jpg")
            plt.show()
            
            plt.clf()
#            plt.plot(sigmas, Jtst)
#            plt.plot(sigmas, Jtrn)
            plt.plot(sigmas, JtstGD)
            plt.plot(sigmas, JtrnGD)
            
            plt.plot(sigmas, np.ones(len(sigmas))*(Jq1f_tst))  # q1c Jtest, Regularization w/ cross-validation
            plt.plot(sigmas, np.ones(len(sigmas))*(Jq1f_trn)) # q1c Jtrn, Regularization w/ cross-validation
            plt.xlabel('Sigma')
            plt.ylabel('Costs')
            plt.legend(['J Test', 'J Train', 'J test q1f', 'J train q1f'])
            plt.title('Cost Function for Various Sigmas')
            plt.savefig('Figures/CostFcn_Q1g.jpg',  format="jpg")
            plt.show()
q1g(True)