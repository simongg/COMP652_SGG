# -*- coding: utf-8 -*-
"""
Created on Fri Sep 21 13:15:48 2018

@author: Simon
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Sep 20 15:21:05 2018

@author: Simon
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 17:09:35 2018

@author: Simon
"""
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
import numpy as np

import matplotlib.pyplot as plt
#Import Linear Regression model from scikit-learn.
import pandas as pd
from matplotlib.pylab import rcParams
rcParams['figure.figsize'] = 12, 10
from sklearn.linear_model import LinearRegression

def linear_regression(data, power, models_to_plot):
    #initialize predictors:
    predictors=['x']
    if power>=2:
        predictors.extend(['x_%d'%i for i in range(2,power+1)])
    
    
    #Fit the model
    linreg = LinearRegression(normalize=True)
    linreg.fit(data[predictors],data['y'])
    y_pred = linreg.predict(data[predictors])
    
    #Check if a plot is to be made for the entered power
    if power in models_to_plot:
        plt.subplot(models_to_plot[power])
        plt.tight_layout()
        plt.plot(data['x'],y_pred,'.')
        plt.plot(data['x'],data['y'],'.')
        plt.title('Plot for power: %d'%power)
    
    #Return the result in pre-defined format
    rss = sum((y_pred-data['y'])**2)
    ret = [rss]
    ret.extend([linreg.intercept_])
    ret.extend(linreg.coef_)
    return ret
 
dim = 9

# Load Data
Y = np.loadtxt('hw1-q1y.csv')
# standardize y dataset
Y = (Y - np.mean(Y)) / np.std(Y)

X = np.loadtxt('hw1-q1x.csv')[:,1]
# Normalize the x variable
X = (X - np.mean(X)) / np.std(X)

data = pd.DataFrame(np.column_stack([X,Y]),columns=['x','y'])
plt.plot(data['x'],data['y'],'.')

for i in range(2,dim+1):  #power of 1 is already there
    colname = 'x_%d'%i      #new var will be x_power
    data[colname] = data['x']**i
print(data.head())

# Split the data
x, xt, y, yt = train_test_split(X, Y, test_size=0.2)

col = ['rss','intercept'] + ['coef_x_%d'%i for i in range(1,16)]
ind = ['model_pow_%d'%i for i in range(1,16)]
coef_matrix_simple = pd.DataFrame(index=ind, columns=col)

#Define the powers for which a plot is required:
models_to_plot = {1:231,3:232,5:233,9:234,12:235,15:236}

#Iterate through all powers and assimilate results
for i in range(1,dim+1):
    coef_matrix_simple.iloc[i-1,0:i+2] = linear_regression(data, power=i, models_to_plot=models_to_plot)
        
