#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 13 23:12:01 2018

@author: simon
"""

# This is a python code created by Simon Geoffroy-Gagnon
# for COMP 652
# This is assignment 1

import numpy as np
import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = [10, 10]

from q1b import q1b
from q1c import q1c
from q1f import q1f
from q1g import q1g
from q1i_Dims import q1i_dim, q1i_l1, q1i_l2

# Set random seed to keep random values not so random
printing = True

np.random.seed(5)
print('Q1b Start')
q1bJtst, q1bJtrn = q1b(printing)
print('Q1b over, start Q1c')
q1cJtst, q1cJtrn = q1c(printing)
print('Q1c over, start Q1f')
q1f(q1bJtst, q1bJtrn, q1cJtst, q1cJtrn)
print('Q1f over, start Q1g')
q1g(printing)
print('Q1g over, start Q1i part 1, dimensions')
q1i_dim(printing)
print('Q1i part 1 over, start Q1i part 2, L2 regression')
q1i_dim(True)
q1i_l2(printing, lambs = [0.01, 0.1, 1, 10])
print('Q1i part 2 over, start Q1i part 3, L1 regression')
q1i_l1(printing, lambs = [0.01, 0.1, 1, 10])
print('Q1i part 3 over.')