# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 17:05:39 2018

@author: Simon
"""
from sklearn.model_selection import train_test_split
import numpy as np
import ML_101_SGG as ml
import matplotlib.pyplot as plt

def q1b(plotting):
    np.random.seed(7)
    maxLambda = 5
    lambdaNum = 100
#    lambdas = np.logspace(-5,maxLambda,lambdaNum)
    lambdas = [0, 1e-1, 1e0, 1e1, 1e2, 1e3, 1e4, 1e5]
    dims = 1

    for Degree in range(dims,dims+1):
        w_star = []
        w_star_norm = []
        Jtst = []
        Jtrain = []
        
        # Load all of the data into memory
        x = np.loadtxt('hw1-q1x.csv')
        y = np.loadtxt('hw1-q1y.csv')
        
        # Normalization code gotten from https://machinelearningmastery.com/scale-machine-learning-data-scratch-python/
        
        # Normalize the x variable
        meansX = ml.column_means(x)
        stdX = ml.column_stdevs(x, meansX)
        # standardize dataset
        ml.standardize_dataset(x, meansX, stdX)
        y = (y - np.mean(y)) / np.std(y)        
        
        x, xt, y, yt = train_test_split(x, y, test_size=0.2)
           
        for lmbd in lambdas:
            w_star.append(ml.get_model_L2(x,y,lmbd))
            Jtst.append(ml.cost_fcn_l2(xt, yt, w_star[-1], lmbd))
            Jtrain.append(ml.cost_fcn_l2(x, y, w_star[-1], lmbd))
            w_star_norm.append(np.linalg.norm(w_star[-1]))
        if plotting:
                
            plt.semilogx(lambdas, w_star)
            plt.xlabel('Lambda')
            plt.ylabel('Weights')
            plt.title("L2 regularization weights for degree %.0f"% Degree)
            plt.savefig('Figures/l2RegWeights_Q1B2.jpg',  format="jpg")
            plt.show()
            
    #        print([np.linalg.norm(v) for v in w_star])
            plt.clf()
            plt.semilogx(lambdas, w_star_norm)
            plt.xlabel('Lambda')
            plt.ylabel('Norm of Weights')
            plt.title("L2 regularization, norm of weights")
            plt.savefig('Figures/l2RegWeightsNorm_Q1B2.jpg',  format="jpg")
            plt.show()
            
            plt.clf()
            plt.semilogx(lambdas, np.sqrt(Jtst))
            plt.semilogx(lambdas, np.sqrt(Jtrain))
            plt.xlabel('Lambda')
            plt.ylabel('Cost RMSE')
            plt.legend(['RMSE Test','RMSE Train'])
            plt.title("RMSE for Various Lambdas with M = %.0f"% Degree)
            plt.savefig('Figures/CostFcn_Q1B2.jpg',  format="jpg")
            plt.show()
            plt.clf()
        print(Jtst)
        return Jtst, Jtrain
q1b(True)