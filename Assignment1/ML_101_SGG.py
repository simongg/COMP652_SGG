# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 16:54:59 2018

@author: Simon
"""
from sklearn.preprocessing import PolynomialFeatures
import numpy as np
from numpy import linalg

# from ML_101_SGG import dimensionalize, get_model_Dim, minmax, normalize, column_means, column_stdevs, standardize_dataset, cost_fcn

# Bunch of helper functions for HW1
def dimensionalize(data, maxdim):
    # Get higher degree
    poly = PolynomialFeatures(degree=maxdim)
    data = poly.fit_transform(data)
    return data

def get_model_Dim(features, labels, lamb):
    if np.ndim(features) != 1:
        n_cols = np.shape(features)[1]
        return linalg.inv(features.transpose().dot(features) + lamb * np.identity(n_cols))\
                .dot(features.transpose()).dot(labels)
    else:
        return linalg.inv(features.transpose().dot(features) + lamb * np.identity(1)).dot(features.transpose()).dot(labels)


def get_model_L2(features, labels, lamb=0.0):
    n_cols = features.shape[1]
    return linalg.inv(features.transpose().dot(features) + lamb * np.identity(n_cols)).dot(features.transpose()).dot(labels)


# Find the min and max values for each column
def minmax(data):
	minmax = list()
	for i in range(len(data[0])):
		col = [row[i] for row in data]
		value_min = min(col)
		value_max = max(col)
		minmax.append([value_min, value_max])
	return minmax

# Rescale dataset columns to the range [0, 1]
def normalize(data, minmax):
	for row in data:
		for i in range(len(row)):
			row[i] = ((row[i] - minmax[i][0]) / (minmax[i][1] - minmax[i][0])) 

# calculate column mean
def column_means(dataset):
	means = [0 for i in range(len(dataset[0]))]
	for i in range(len(dataset[0])):
		col_values = [row[i] for row in dataset]
		means[i] = sum(col_values) / float(len(dataset))
	return means
	
# calculate column standard deviations
def column_stdevs(dataset, means):
	stdevs = [0 for i in range(len(dataset[0]))]
	for i in range(len(dataset[0])):
		variance = [pow(row[i]-means[i], 2) for row in dataset]
		stdevs[i] = sum(variance)
	stdevs = [np.sqrt(x/(float(len(dataset)-1))) for x in stdevs]
	return stdevs

# standardize dataset
def standardize_dataset(dataset, means, stdevs):
	for row in dataset:
		for i in range(len(row)):
			row[i] = (row[i] - means[i]) / stdevs[i]
            
def cost_fcn_l2(X, Y, B, lamb = 0):
    m = len(X)
    J = np.sum((np.matmul(X,B) - Y).T*(np.matmul(X,B) - Y))/(2 * m) + 1/(2 * m)*lamb*np.matmul(B.T,B)
#    print(J)
    return J

def cost_fcn_l1(X, Y, B, lamb = 0):
    m = len(X)
    J = np.sum((np.matmul(X,B) - Y).T*(np.matmul(X,B) - Y))/(2 * m) +lamb*sum(abs(B))
#    print(J)
    return J
    
def gradient_descent(gradientDescentInputs, gradientDescentTargets, B, alpha, iterations, lamb=0, l = False):

    cost_history = [0] * iterations
    m = len(gradientDescentTargets)
    for iteration in range(iterations):
        # Hypothesis Values
        h = gradientDescentInputs.dot(B)
        # Difference b/w Hypothesis and Actual Y
        loss = h - gradientDescentTargets
        # Gradient Calculation
        if not(lamb) and not(l):
            gradient = gradientDescentInputs.T.dot(loss) / m
            B = B - alpha * gradient
        elif l == 'l2':
                gradient = gradientDescentInputs.T.dot(loss) / m + lamb * (B ** 2)
                B = B - alpha * gradient
        elif l == 'l1':
                gradient = gradientDescentInputs.T.dot(loss) / m + lamb*np.sign(B)
                    
#        print(iteration)
        # Changing Values of B using Gradient
        
        
        # New Cost Value
        if l == 'l2':
            cost = cost_fcn_l2(gradientDescentInputs, gradientDescentTargets, B, lamb)            
        elif l == 'l1':
            cost = cost_fcn_l1(gradientDescentInputs, gradientDescentTargets, B, lamb)
        else:
            cost = cost_fcn_l2(gradientDescentInputs, gradientDescentTargets, B, lamb)
            
        cost_history[iteration] = cost
    return B, cost_history
