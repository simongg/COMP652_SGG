# -*- coding: utf-8 -*-
"""
Created on Thu Sep 20 16:29:57 2018

@author: Simon
"""

import numpy as np
import ML_101_SGG as ml
import matplotlib.pyplot as plt
from q1a import q1a

def q1b_dims():
    maxDim = 15
    dims = np.linspace(1,maxDim,maxDim)
    alpha = 0.01
    RMSEtst = []
    RMSEtrn = []
    for dim in dims:
        X, x_test, Y, y_test = q1a(int(dim), 0.2)
        # Generate x, y
        Xshape = np.shape(X)
        B = np.zeros(Xshape[1])
        Y = np.array(Y)

        inital_cost = ml.cost_fcn(X, Y, B)
        
        # Training
        newB, cost_history = ml.gradient_descent(X, Y, B, alpha, 100000)
        print(inital_cost)
        print(cost_history[-1])

        # Testing
        test_cost = ml.cost_fcn(x_test, y_test, newB)
        trn_cost = ml.cost_fcn(X,Y, newB)
        print(test_cost)
        print('\n')
        RMSEtst.append(np.sqrt(test_cost))
        RMSEtrn.append(np.sqrt(trn_cost))
    plt.plot(np.append(np.roll(RMSEtst,1), RMSEtst[maxDim - 1]))
    plt.plot(np.append(np.roll(RMSEtrn,1), RMSEtrn[maxDim - 1]))
    ax = plt.subplot(111)
    ax.set_xlim(1, maxDim)
    plt.xticks(dims)
    plt.legend(['Test','Train'])
    plt.title('Test/Train RMSE Score')
    plt.ylabel('RMSE')
    plt.xlabel('M')