# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 17:03:06 2018

@author: Simon
"""
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures
import numpy as np
import ML_101_SGG as ml

def q1a(Degree, testSz):
    # Load all of the data into memory
    x = np.loadtxt('hw1-q1x.csv')
    y = np.loadtxt('hw1-q1y.csv')
    
    # Normalize the x variable
    meansX = ml.column_means(x)
#    meansY = column_means(y)
    
    stdX = ml.column_stdevs(x, meansX)
#    stdY = column_stdevs(y, meansY)
      
    # standardize dataset
    ml.standardize_dataset(x, meansX, stdX)
#    standardize_dataset(y, meansY, stdY)
    
    # Get higher degree
    poly = PolynomialFeatures(degree=Degree)
    X = poly.fit_transform(x)
    
    # Split data
    X, x_test, Y, y_test = train_test_split(X, y, test_size=testSz)
    return X, x_test, Y, y_test
