# -*- coding: utf-8 -*-
"""
Created on Tue Oct  2 18:23:02 2018

@author: Simon
"""
import numpy as np
import matplotlib.pyplot as plt

rhos = [0.1, 0.5, 1]

d = 150

x = np.linspace(-1,1,100)

for rho in rhos:
    for ii in range(1,d):
        y = np.exp(-x**2/(2*rho**2))*x**(ii - 1)/(rho**(ii-1)*np.math.sqrt(np.math.factorial(ii - 1)))
        plt.plot(x,y)
    plt.title('Gaussian Taylor Expansion, d = 150, rho={}'.format(rho))
    plt.xlabel('x')
    plt.ylabel('y')
    plt.savefig('Figures/q5b_rho{}.jpg'.format(rho*10),  format="jpg")
    plt.show()

    w = ones(len(y))
