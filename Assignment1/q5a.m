%% Q5a 
% Create 3 linear functions
close all

x = linspace(-1,1);

plot(x,-0.33*x)
hold on
plot(x,0.125*x)
plot(x,0.5*x)

title('Linear Functions from k(u,v) = u*v')
xlabel('\chi')
ylabel('Functions')

saveas(gcf, 'Figures/q5KernelFunctions.png')
hold off
%% Save norm plot
v = x'*x;
vec = vecnorm(v);
plot(x,vec)
hold on

plot(-.33, norm(-.33*x), 'r*')
plot(0.125, norm(0.125*x), 'r*')
plot(0.5, norm(0.5*x), 'r*')

title('Norm for each K value')
xlabel('\chi')
ylabel('Norm')


saveas(gcf, 'Figures/normQ5.png')