# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 17:09:35 2018

@author: Simon
"""
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold 
from sklearn import preprocessing
import numpy as np
import operator
import ML_101_SGG as ml
import matplotlib.pyplot as plt

 
def q1il2(plotting = True, alpha = 0.0001, iterations = 5000, lambs = [0.01, 0.1, 1, 10, 100, 1000]):
    #np.random.seed(7)
    w_star = []
    w_star_norm = []
    Jtrain = []
    Jvalidation = []
    Jtest = []
    dim = 9
    lambs = [0.01, 0.1, 1, 10, 100, 1000, 10000]
    for lamb in lambs:
        # Load Data
        Y = np.loadtxt('hw1-q1y.csv')
        # standardize y dataset
        Y = (Y - np.mean(Y)) / np.std(Y)
        
        X = np.loadtxt('hw1-q1x.csv')
        # Normalize the x variable
        meansX = ml.column_means(X)
        stdX = ml.column_stdevs(X, meansX)
        # standardize dataset
        ml.standardize_dataset(X, meansX, stdX)
        
        # Get only the 2nd column
        X = X[:,1].flatten().reshape(-1,1)
        
        # Dimensionalize X
        x = ml.dimensionalize(X, dim)
        
        # Split the data
        x, xt, y, yt = train_test_split(x, Y, test_size=0.2)
        x = preprocessing.scale(x)
        xt = preprocessing.scale(xt)
        y = preprocessing.scale(y)
        yt = preprocessing.scale(yt)


        kf = KFold(n_splits=5, random_state=None, shuffle=True) # Define the split - into 5 folds 
        kf.get_n_splits(x)
        Jval = []
        w_star_curr = []
        Jtrn = []
        Jval = []
        for train_index, valid_index in kf.split(x):
            B = np.zeros(np.shape(x)[1])
            Y = np.array(y)
            
            newB, cost_history = ml.gradient_descent(x[train_index], Y[train_index], B, alpha, iterations, lamb, l = 'l2')
            w_star_curr.append(newB)
            
            Jval.append(ml.cost_fcn_l2(x[valid_index], y[valid_index], w_star_curr[-1], lamb))
            Jtrn.append(ml.cost_fcn_l2(x[train_index], y[train_index], w_star_curr[-1], lamb))
            
        # Get average cost for validation
        Jvalidation.append(np.mean(Jval))
        Jtrain.append(np.mean(Jtrn))
        
        min_j_idx, min_j = min(enumerate(Jval), key=operator.itemgetter(1))
    
        # Append Cost w/ smallest validation cost
        Jtest.append(ml.cost_fcn_l2(xt, yt, w_star_curr[min_j_idx]))
    
        # Append optimal weights
        w_star.append(w_star_curr[min_j_idx])
        w_star_norm.append(np.linalg.norm(w_star[-1]))
    if plotting:
            
        plt.semilogx(lambs, w_star_norm)
        plt.xlabel('Dimension')
        plt.ylabel('Norm of Weights')
        plt.title("Norm of Weights")
        plt.legend("Norm")
        plt.savefig('Figures/CVNormWeights_Q1i_l2.jpg',  format="jpg")
        plt.show()
        
        plt.clf()
        plt.semilogx(lambs, (Jtest))
        plt.semilogx(lambs, (Jvalidation))
        plt.semilogx(lambs, (Jtrain))
        plt.xlabel('Regularization Parameter')
        plt.ylabel('Cost')
        plt.legend(['J Test','J Validation', 'J Train'])
        plt.title("Cost Function for Various Regularization Parameters")
        plt.savefig('Figures/CostFcn_Q1i_l2.jpg',  format="jpg")
        plt.show()
            
        plt.semilogx(lambs, w_star)
        plt.xlabel('Lambda')
        plt.ylabel('Weights')
        plt.title("Regression Weights")
        plt.savefig('Figures/RegWeights_Q1i_l2.jpg',  format="jpg")
        plt.show()
        
        Xplot = np.linspace(min(x[:,1]), max(x[:,1]), 100)
        
        for lambIdx, _ in enumerate(lambs):
            Y9 = w_star[lambIdx][0] + Xplot * w_star[lambIdx][1] + (Xplot ** 2) * w_star[lambIdx][2] + (Xplot ** 3) * w_star[lambIdx][3] + (Xplot ** 4) * w_star[lambIdx][4] + (Xplot ** 5) * w_star[lambIdx][5] + (Xplot ** 6) * w_star[lambIdx][6] + (Xplot ** 7) * w_star[lambIdx][7] + (Xplot ** 8) * w_star[lambIdx][8] + (Xplot ** 9) * w_star[lambIdx][9]
            plt.plot(Xplot, Y9)
          
        plt.scatter(x[:, 1], y)       
        plt.scatter(xt[:, 1], yt)        
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.title("Fittings, L2 regression")
        plt.legend(['Lamb = 0.01','Lamb = 0.1','Lamb = 1','Lamb = 10','Scatter Train', 'Scatter Test'])
        plt.savefig('Figures/Fittings_Q1i_l2.jpg',  format="jpg")
        plt.show()
#q1il2()