# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 17:08:41 2018

@author: Simon
"""
from sklearn.model_selection import train_test_split
import numpy as np
import ML_101_SGG as ml
from sklearn import preprocessing
import matplotlib.pyplot as plt
from q1e import q1e

def q1f(q1bJtst, q1bJtrn, q1cJtst, q1cJtrn, plotting = False):
#        np.random.seed(7)
        w_gd = []
        w_gd_norm = []
        w_star = []
        w_star_norm = []
        Jtrn = []
        Jtst = []      
        JtstGD = []
        JtrnGD = []
        
        sigmas = [0.1, 0.5, 1, 5]
#        sigmas = np.linspace(0.1, 10, 100)
        # No Lambda
        lmbd = 0
        
        for sigma in sigmas:
            # Load Data
            x = np.loadtxt('hw1-q1x.csv')
            y = np.loadtxt('hw1-q1y.csv')
            
            # Normalize the x and y variables
            x = preprocessing.scale(x)
            y = preprocessing.scale(y)
            
            x = q1e(x,sigma)
#            print(np.mean(x))
            # Split X and Y data
            x, xt, y, yt = train_test_split(x, y, test_size=0.2)
            B = np.zeros(np.shape(x)[1])
            Y = np.array(y)
#            inital_cost = cost_fcn_l2(x, Y, B)
            
            w_star.append(ml.get_model_L2(x,y,lmbd)) 
            # Training
            newB, cost_history = ml.gradient_descent(x, Y, B, 0.001, 5000)          # Gradient descent gives the same results as Closed Form
#            print(inital_cost)
#            print(np.sqrt(cost_history[-1]))
            w_gd.append(newB)
            
            Jtst.append(ml.cost_fcn_l2(xt, yt, w_star[-1], lmbd))
            Jtrn.append(ml.cost_fcn_l2(x, y, w_star[-1], lmbd))
            
            JtstGD.append(ml.cost_fcn_l2(xt, yt, newB, lmbd))
            JtrnGD.append(cost_history[-1])
            
            # Append optimal weights
            w_star_norm.append(np.linalg.norm(w_star[-1]))
            w_gd_norm.append(np.linalg.norm(w_gd[-1]))
        
        if plotting:
#            plt.plot(sigmas, w_star_norm)
            plt.plot(sigmas, w_gd_norm)
            plt.xlabel('Sigma')
            plt.ylabel('Norm of Weights')
            plt.title("Regression Weights")
            plt.legend(["Gradient Descent Weights"])
            plt.savefig('Figures/RegWeightsNorm_Q1F.jpg',  format="jpg")
            plt.show()
            
#            plt.plot(sigmas, w_star)
            plt.plot(sigmas, w_gd)
            plt.xlabel('Sigma')
            plt.ylabel('Weights')
            plt.title("Regression Weights")
            plt.savefig('Figures/RegWeights_Q1F.jpg',  format="jpg")
            plt.show()
            
            plt.clf()
#            plt.plot(sigmas, Jtst)
#            plt.plot(sigmas, Jtrn)
            plt.plot(sigmas, JtstGD)
            plt.plot(sigmas, JtrnGD)
#            print(min(JtstGD))
#            print(min(JtrnGD))
            plt.plot(sigmas, np.ones(len(sigmas))*min(q1bJtst)) # q1b Jtest, Regularization w/o cross-validation
            plt.plot(sigmas, np.ones(len(sigmas))*min(q1bJtrn)) # q1b Jtrn, Regularization w/o cross-validation
            plt.xlabel('Sigma')
            plt.ylabel('Costs')
            plt.legend(['J Test', 'J Train', 'Min J test Q1b', 'Min J train Q1b'])
            plt.title('Cost Function for Various Sigmas')
            plt.savefig('Figures/CostFcn_Q1F_Good.jpg',  format="jpg")
            plt.show()
q1f([0.1447257180393214, 1], [0.09923697342737674, 1], 0, 0, True)