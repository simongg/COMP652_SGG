# -*- coding: utf-8 -*-
"""
Created on Sat Sep 22 14:57:59 2018

@author: Simon
"""

from scipy.stats import beta
import numpy as np
import matplotlib.pyplot as plt

a = 50
b = 50
x = np.linspace(0,1,1000)
RV = beta(a, b)

plt.plot(x, beta.pdf(x, a, b))
plt.title('Beta Distribution, a = 50, b = 50')
plt.ylabel('Probability Density')
plt.savefig('Figures/beta_distribution.jpg',format='jpg')

a = 53
b = 50
x = np.linspace(0,1,1000)
RV = beta(a, b)

plt.plot(x, beta.pdf(x, a, b))
plt.title('Prior (a = 50, b = 50) and Posterior (a = 53, b = 50) Beta Distribution')
plt.ylabel('Probability Density')
plt.savefig('Figures/beta_distribution_posterior.jpg',format='jpg')
plt.legend(['Prior','Posterior'])

