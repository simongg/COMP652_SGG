# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 17:07:24 2018

@author: Simon
"""
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold 
from sklearn import preprocessing
import numpy as np
import ML_101_SGG as ml
import matplotlib.pyplot as plt
import operator

def q1c(plotting):
    np.random.seed(7)
    maxLambda = 5
    lambdaNum = 100
#    lambdas = np.logspace(-5,maxLambda,lambdaNum)
    lambdas = [0, 1e-1, 1e0, 1e1, 1e2, 1e3, 1e4, 1e5]
    
    w_star = []
    w_star_norm = []
    Jtrain = []
    Jtest = []      
    Jvalidation = []

    # Load all of the data into memory
    x = np.loadtxt('hw1-q1x.csv')
    y = np.loadtxt('hw1-q1y.csv')
    
    # Normalization code gotten from https://machinelearningmastery.com/scale-machine-learning-data-scratch-python/
    
    # Normalize the x variable
    meansX = ml.column_means(x)
    stdX = ml.column_stdevs(x, meansX)
    # standardize dataset
    ml.standardize_dataset(x, meansX, stdX)
    y = (y - np.mean(y)) / np.std(y)   
        
    # Split X and Y data
    x, xt, y, yt = train_test_split(x, y, test_size=0.2)
    
    # Train ML w/ kfold=5
    for lmbd in lambdas:
        kf = KFold(n_splits=5, random_state=None, shuffle=True) # Define the split - into 5 folds 
        kf.get_n_splits(x)
        Jval = []
        Jtrn = []
        w_star_curr = []
            
        for train_index, valid_index in kf.split(x):

            w_star_curr.append(ml.get_model_L2(x[train_index],y[train_index],lmbd))
            
            Jval.append(ml.cost_fcn_l2(x[valid_index], y[valid_index], w_star_curr[-1], lmbd))
            Jtrn.append(ml.cost_fcn_l2(x[train_index], y[train_index], w_star_curr[-1], lmbd))
        
        # Get average cost for validation
        Jvalidation.append(np.mean(Jval))
        Jtrain.append(np.mean(Jtrn))
        
        min_j_idx, min_j = min(enumerate(Jval), key=operator.itemgetter(1))

        # Append Cost w/ smallest validation cost
        Jtest.append(ml.cost_fcn_l2(xt, yt, w_star_curr[min_j_idx]))

        # Append optimal weights
        w_star.append(w_star_curr[min_j_idx])
        w_star_norm.append(np.linalg.norm(w_star[-1]))
    if plotting:
        
        plt.semilogx(lambdas, w_star_norm)
        plt.xlabel('Lambda')
        plt.ylabel('Norm of Weights')
        plt.title("Norm of Weights")
        plt.savefig('Figures/l2CVNormWeights_Q1C.jpg',  format="jpg")
        plt.show()
        
        plt.semilogx(lambdas, w_star)
        plt.xlabel('Lambda')
        plt.ylabel('Weights')
        plt.title("Weights")
        plt.savefig('Figures/l2CVWeights_Q1C.jpg',  format="jpg")
        plt.show()
        
        plt.clf()
#        plt.semilogx(lambdas, np.sqrt(Jtest))
        plt.semilogx(lambdas, np.sqrt(Jvalidation))
        plt.semilogx(lambdas, np.sqrt(Jtrain))
        plt.xlabel('Lambda')
        plt.ylabel('RMSE')
        plt.legend(['RMSE Validation', 'RMSE Train'])
        plt.title("RMSE Cost Function for Various Lambdas")
        plt.savefig('Figures/RMSE_CostFcn_Q1C_mean.jpg',  format="jpg")
        plt.show()
    
    print(Jvalidation)
    return Jtrain
jtrn = q1c(True)