\documentclass[letterpaper, 12pt, oneside]{report}
\usepackage[margin=0.75in]{geometry}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{caption}
\usepackage[normalem]{ulem}
\usepackage{sectsty}
\usepackage{float}
\usepackage{setspace}
\usepackage{amsmath}

\newcommand\tab[1][1cm]{\hspace*{#1}}

\hypersetup{
	colorlinks=true,
	linkcolor=red}

\sectionfont{\underline}
\def\thesection{\arabic{section}}
\setlength{\parskip}{1em}
\pagestyle{plain}
\onehalfspacing

% Title Page
\title{COMP 652 - Assignment 3 Report}
\author{Simon Geoffroy-Gagnon - 260856157}
\date{October 22nd, 2018}

% Document
\begin{document}
\maketitle
\section{Introduction}

This is a report on the third assignment for COMP 652.

\section*{Question 1}
\subsection*{a)}
The difference between a linear bandit and a k-armed bandit is essentially that we have infinitely many actions in a linear bandit and a finite number of actions ($K$ actions, to be precise) in the k-armed bandit.\\
For the linear bandit, the structured and continuous action space is $\chi$, with $x\in\chi$. There are an infinite amount of different possible actions. Since this is a linear bandit setting, we also have an unknown parameter $\theta_\star$, where $f(x)=\langle x,\theta_\star\rangle$, and we want to maximize $f(x)$ by always choosing $x_\star$:
\begin{equation}
    x_\star = \underset{x\in\chi}{argmax}\;\langle x,\theta_\star\rangle
\end{equation}
We also get some noise in our returned data, denoted as $\epsilon$. In this case, we can assume our noise is Gaussian: $\epsilon = N(0,\sigma)$. In the end, we want to maximize the rewards from the following equation:
\begin{equation}
    f(x) = \langle x,\theta_\star\rangle + \epsilon_t
\end{equation}
by choosing $x_\star$ at every round. \\
Additionally, for a linear bandit setting, we require the notion of similarity, which implies continuity and differentiability:
\begin{equation}
   \{\theta_\star|\theta_i < \infty, \forall i\}
\end{equation}
On the other hand, a k-armed bandit simply involves $K$ possible actions --- this means we have a disjoint action space, $k\in K$, as opposed to a structured and continuous action space $x \in \chi$ as in the linear bandit setting. Each action is assigned a different mean, $\mu_k$, and we want to maximize our return by picking the action with the maximum mean, $\mu_\star$. If we have a Gaussian k-armed bandit, we wish to maximize the following:
\begin{equation}
    \sum \mu_{(k_t)} = N(\mu_k, \sigma_k)
\end{equation}
by choosing the bandit with the optimal mean, $\mu_\star$:
\begin{equation}
    k_\star = \underset{\kappa\in K}{argmax}\;\mu_{k_t}(\nu)
\end{equation}\\
\if 0
\newpage
\begin{tabular}{|c|c|}
    \hline 
    K-Armed Bandit & Linear Bandit \\ 
    \hline Action Set: $\kappa\in \{1,2,3\dots K\}$,&\\
     where $K$ is the number of bandits & Action Space: $\chi \subseteq R^n$  \\
    Reward: $K_\star = argmax_{\kappa\in K} \mu_{k_t}(\nu)$ & Reward: $x_\star = argmax_{x\in\chi}\langle x,\theta_\star\rangle$ \\
    Does not require the notion of similarity & Requires the notion of similarity.\\& This means that the linear function\\& needs to be continuous and differentiable:\\& $\{\theta_\star|\theta_i < \infty \forall i\}$\\
    \hline
\end{tabular} 
\fi
With these notions, we can explicit the linear bandit to a k-armed bandit by seeing that, since the noise of our linear bandit, $\epsilon_t$, is a normal Gaussian,
\begin{equation}
    \epsilon_t = N(0,\sigma_t)
\end{equation}
added to a real value,
\begin{equation}
\langle x,\theta_\star\rangle
\end{equation}
We can simply use that real value as the mean of our Gaussian noise, to get:
\begin{equation}
    f(x) = N(\langle x,\theta_\star\rangle,\sigma)
\end{equation}
since adding a real value to a normally distributed Gaussian random variable simply changes the mean of the normally distributed Gaussian random variable.
This is equivalent to our k-armed bandit, where
\begin{equation}
    k_t = N(\mu,\sigma)
\end{equation}
Where $\mu = \langle x,\theta_\star\rangle$, and $\sigma$ remains the same. Instead of having continuous values for $x\in\chi$, we have discrete values for $x$, where each $x$ is a different $k$ in $K$. This means that, if we pick $K$ different points in $\chi$, and continuously sample them, we end up with a K-armed bandit from the original linear bandit setting.
\subsection*{b)}
\underline{K-armed Bandit regret equation:}
\begin{equation}
    R_T(\pi,\nu) = T*\mu_\star - \sum_{t=1}^{T}\mu_{(k_t)}(\nu)
\end{equation}
where $\mu_\star = {max}_{k\in K}\mu_k(\nu)$.\\
\underline{Linear Bandit Regret equation:}
\begin{equation}
    R_T(\pi,\theta_\star) = \sum_{t=1}^T\langle x_\star,\theta_\star\rangle - \sum_{t=1}^T\langle x,\theta_\star\rangle
\end{equation}
where $\langle x_\star,\theta_\star\rangle$ is the optimal play.\\
We assume $x_\star$ and $\theta_\star$ remains constant up to the horizon, which means:
\begin{equation}
\sum_{t=1}^T\langle x_\star,\theta_\star\rangle = T*\langle x_\star,\theta_\star\rangle
\end{equation}
We can clearly see that $\mu_\star = \langle x_\star,\theta_\star\rangle$, and $\mu_{k_t} = \langle x,\theta_\star\rangle$, which means:
\begin{equation}
R_T(\pi,\theta_\star) = \sum_{t=1}^T\langle x_\star,\theta_\star\rangle - \sum_{t=1}^T\langle x,\theta_\star\rangle = T*\langle x_\star,\theta_\star\rangle- \sum_{t=1}^T\langle x,\theta_\star\rangle = T*\mu_\star - \sum_{t=1}^{T}\mu_{k_t}
\end{equation}
\subsection*{c)}
The OFUL algorithm is simply a larger version of the UCB algorithm.\\
If we have a k-armed bandit, we get the UCB by sampling all of our bandits once. We then proceed on taking the samples from the highest mean we currently have. Eventually, by law of large numbers, we should get $\hat{\mu} = \mu_\star$ through getting $\hat{k} = k_\star$.\\ 
In OFUL, we start by taking an arbitrarily large amount of samples, each separated by a constant value, from our linear function $\langle x,\theta_\star\rangle$. For our next turn, we sample the $x$ value that has the maximum mean. Again, by law of large numbers, we should eventually get $\hat{x} = x_\star$.\\
\section*{Question 2}
\subsection*{a)}
A 2D spin glass model, with pixels connected in a 4-neighborhood, would have the following distribution:
\begin{equation}
    \Pr(x_1,x_2,x_3,\dots,x_n) = \frac{1}{Z}\prod_{i\sim  j}^{1-4}\phi_{i,j}(x_i,x_j)
\end{equation}
where $Z$ would be a factor to keep the total probability equal to 1 and $i\sim j$ are the nodes that are that are neighbors (i.e. connected) within our graph (see figure \ref{fig:q2a1}).\\
If we were to connect the nodes in an 8-neighborhood, we would get the following equation:
\begin{equation}
    \Pr(x_1,x_2,x_3,\dots,x_n) = \frac{1}{Z}\prod_{i\sim  j}^{1-8}\phi_{i,j}(x_i,x_j)
\end{equation}
Where we would have twice the amount of connected nodes - we would therefore have to change $Z$ accordingly. 
So for a single node $x_1$, the probability of $x_1$ would only rely on nodes $x_2$ to $x_9$ (see figure \ref{fig:q2a2}).
\begin{figure}[H]
    \centering
	\fbox{\includegraphics[width=0.5\textwidth]{../Figures/4Neighbor}}
	\caption{4 neighbor Markov Random field, where the green lines are the causal relations to $x_1$}
	\label{fig:q2a1}
\end{figure}
\begin{figure}[H]
    \centering
	\fbox{\includegraphics[width=0.5\textwidth]{../Figures/8Neighbor}}
	\caption{8 neighbor Markov Random field, where the green lines are the causal relations to $x_1$}
	\label{fig:q2a2}
\end{figure}

\subsection*{b)}
The advantages and disadvantages of using an 8-neighborhood rather than a 4-neighborhood relates back to the bias-variance trade-off that always occurs when we change the complexity of our system. In this case, relating the current pixel to twice as many neighboring pixels will increase biasing and decrease variance, as it is a more global averaging of our entire system. The computation time would also increase since we have to take into account more variables when looking at each pixel and its distribution.
\subsection*{c)}
This is the Gibbs sampling algorithm using a non-negative linear energy functions and evidence injected along the leftmost edge.\\\\
\textbf{1)} Start by initializing the leftmost variables to our evidence values: $x_1,x_2,x_3 = e_1,e_2,e_3$.\\
\textbf{2)} Start all other variables at 0\\
\textbf{3)} Until convergence, repeat:\\
for i = 2:n\\
\tab for j = 1:n\\
\tab[2cm] $x_{i,j}' = \Pr(X_{i,j}|markovBlanket(x_{i,j})) \propto\frac{1}{Z}\prod_{i,j\sim k,l}\psi(x_{i,j},x_{k,l})$\\
       % $ = \frac{1}{Z}\prod_{i,j\sim  k,l}W_{(i,j),(k,l)}*(x_{i,j}-x_{k,l})^2$

Where $markovBlanket()$ is the markov blanket of our undirected graph, which is simply the closest neighbors of the current variable being updated, $(i,j)\sim (k,l)$. $\Pr\left(X_{i,j}|markovBlanket(x_{i,j})\right)$ is proportional to $\frac{1}{Z}\prod_{i\sim j}\psi(x_{i,j},x_{k,l})$. $Z$ is the normalizing term used to ensure that the total probability of a node is equal to 1: $Z = \sum_{(i,j)\sim (k,l)}\psi(x_{i,j},x_{k,l})$.\\ $\psi(x_{i,j},x_{k,l})$ is a linear energy function. Since we need a non-negative energy function we can use, as an example, $\frac{1}{Z}\prod_{i,j\sim  k,l}W_{(i,j),(k,l)}*(x_{i,j}-x_{k,l})^2$. Any other non-negative functions will work however.
\begin{figure}[H]
    \centering
    \fbox{\includegraphics[width=0.5\textwidth]{../Figures/q2c}}
    \caption{Gibb's sampling of a 4-neighbor Ising model, with the left-most edge being observed variables (i.e. constants). In our algorithm the first R.V. we set is $x_{2,1}$. We get a value of $x_{i,j} \sim\Pr(markovBlanket(x_{i,j}))=\prod_{i,j\sim k,l}W_{(i,j),(k,l)}(x_{(i,j)}-x_{(k,l)})^2$, with $x_{1,1}$ being the observed value, and $x_{2,2}$ and $x_{3,1}$ being set to any arbitrary value since we have not sampled them yet.}
    \label{fig:q2c}
\end{figure}

\section*{Question 3}
\subsection*{a)}
\begin{figure}[H]
    \centering
    \fbox{\includegraphics[width=0.3\textwidth]{../Figures/q3a}}
    \caption{Belief Network of the Drug Age Gender Recovery problem}
    \label{fig:q3a}
\end{figure}

\begin{equation}
    \Pr(R,A,G,D) = \Pr(R|A,G,D)*\Pr(D|A,G)*\Pr(A)*\Pr(G)
\end{equation}
The likelihood of recovery if we know that the patient is administered the drug is:
\begin{equation}
    \Pr(R = 1|D = 1) = \frac{\sum_{G,A}\Pr(\Pr(R=1|A,G,D=1)*\Pr(D=1|A,G)*\Pr(A)*\Pr(G))}{\sum_{R,G,A}\Pr(R|A,G,D=1)*\Pr(D=1|A,G)*\Pr(A)*\Pr(G)}
\end{equation}

The likelihood of recovery if we know that the patient is young \textbf{and} is administered the drug is:
\begin{equation}
    \Pr(R = 1|D = 1) = \frac{\sum_{G}\Pr(\Pr(R=1|A=1,G,D=1)*\Pr(D=1|A=1,G)*\Pr(A=1)*\Pr(R,G))}{\sum_{G,A}\Pr(R|A,G,D=1)*\Pr(D=1|A,G)*\Pr(A=1)*\Pr(G)}
\end{equation}
Which can be simplified to:
\begin{equation}
\Pr(R = 1|D = 1) = \frac{\sum_{G}\Pr(\Pr(R=1|A=1,G,D=1)*\Pr(D=1|A=1,G)*\Pr(G))}{\sum_{G,A}\Pr(R|A,G,D=1)*\Pr(D=1|A,G)*\Pr(G)}
\end{equation}
\subsection*{b)}
\begin{figure}[H]
    \centering
    \fbox{\includegraphics[width=0.5\textwidth]{../Figures/q3b}}
    \caption{Belief Network of the factorized joint distribution $A\leftarrow B\leftarrow C$}
    \label{fig:q3b}
\end{figure}
With the equation of the distribute
Since A,B,C are binary, the parameter space contains $2^3=$ \textbf{8 different parameters}, and this is what we would get if we make no assumptions on the joint distribution.\\
On the other hand, by making the assumption on the joint distribution, we come down to only \textbf{5 parameters}, as seen in the equation below.
\begin{equation}
	P = \sum_{\nu}(n_\nu-1)\prod_{u\in pa(\nu)}n_u = (2 - 1) + (2-1)*2 + (2-1)*2 = 1 + 2 + 2 = 5
\end{equation}
where $\nu$ are each nodes, $n_\nu$ are the number of parameters at each nodes, and $u\in pa(\nu)$ are the nodes parents.
\section*{Question 4}
\subsection*{a) A binary classification of 1000 training examples, each containing 6 dimensions.}
Since this is a binary classification with a moderate amount of testing examples and low dimensionality, I would simply use a kernelized linear classification (since we do not know if these examples are linearly distributed) where we would fit a line separating the two classes. Since we use a kernel, we would probably also use a regularizer since we do not want to over-fit the data.
\subsection*{b) A binary classification of 1000 training examples, each containing 10,000 dimensions.}
Having 10,000 dimensions but only 1000 training example will make over-fitting likely if we do not find a way to lower the dimensionality of this system. Additionally, having 10,000 dimensions is quite computationally expensive --- we can therefore use an RKHS to handle the large dimensionality, as RKHS is very useful when $d\to+\infty$. Since we know that this data is linear, we can use the Dot Product kernel. We will also use an L1 regularizer with our (kernelized) linear classification to classify our data into two separate classes.
\subsection*{c) Predicting stock market values based on previous values.}
Predicting values form past values, some more recent than others, seems like the perfect job for a recursive neural network as it involves sequential data. I would use an LSTM RNN, as this is an efficient neural network architecture when determining time series, such as predicting stock indexes over the 3 following days using data from the previous week. GRUs would also work, but LSTMs and GRUs work equally well with large amounts of data - something which the stock market definitely has.
\subsection*{d) Failure Likelihood and random noise}
For this problem I would likely use a Gaussian Process, combining the adequate kernels with a white Kernel. This would be useful as I could determine the 85\% failure likelihood by looking at the standard deviation of the fitted lifetime curve. Furthermore, I would know the random failure rate, so I would set the white kernel to take this into account, thereby getting a curve fitting algorithm that implements likelihood. 




%% table for reference
%\begin{table}[H]
%	\centering
%	\begin{tabular}{|c| c c c c c |c|}
%		\hline L
%		&\textbf{N}&10&100&1000&10000&Theoretical Values\\\hline
%		\textbf{In-phase component $X_{I,n}$}&$\beta_1$&0.067&0.0054&0.0171&1.69$\times 10^-4$&0\\
%		&$\beta_2$&1.9&2.5436&2.7142&2.7331&3\\\hline
%		Quadrature component $X_{Q,n}$&$\beta_1$&0.0104&0.0373&2.96$\times 10^-6$&8.31$\times 10^-5$&0\\
%		&$\beta_2$&1.6802&2.7498&2.7996&2.6838&3\\\hline
%	\end{tabular}
%	\caption{$\beta$ Values Calculated }
%	\label{table:valueTable}
%\end{table}

%\begin{lstlisting}[style=Matlab-editor]
%code here
%\end{lstlisting}

%\subsection{Results Part b}
%\begin{figure}[H]
%	\centering
%	\fbox{\includegraphics[width=0.7\textwidth]{Figures/hist_zi_gaus}}
%	\caption{Histogram of $Z_I$}
%	\label{fig:hist_zi}
%\end{figure}

\end{document}
