# Load LSTM network and generate text
import sys
import numpy
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.callbacks import ModelCheckpoint
from keras.utils import np_utils


# load ascii text and covert to lowercase
filename = "Raw Data/AliceInWonderland_Raw1.txt" # Alice in wonderland
book = "AiW"
#filename = "Raw Data/The Lord Of The Ring 1-The Fellowship Of The Ring_djvu.txt" # LOTR Fellowship of the ring
#book = "LotR_FotR"
#filename = "Raw Data/The Lord Of The Ring 2-The Two Towers_djvu.txt" # LOTR The Two Towers
#book = "LotR_TTT"
#filename = "Raw Data/The Lord of the Rings 3 - The Return Of The King_djvu.txt" # LOTR The Return of the King
#book = "LotR_TRotK"


raw_text = open(filename, encoding="utf8").read()
raw_text = raw_text.lower()

unwanted_chars = ('\r', '"', "'", '(', ')', '*', ':', ';',\
 '[', ']', '_','\xbb', '\xbf', '\xef','®', '=', '\n')


# Remove all special characters
raw_text_no_spec_char = "".join(c for c in raw_text if c not in unwanted_chars)

# create mapping of unique chars to integers
chars = sorted(list(set(raw_text_no_spec_char)))
char_to_int = dict((c, i) for i, c in enumerate(chars))
n_chars = len(raw_text_no_spec_char)
n_vocab = len(chars)
print("Total Characters: ", n_chars)
print("Total Vocab: ", n_vocab)
	

int_to_char = dict((i, c) for i, c in enumerate(chars))

# create mapping of unique chars to integers
chars = sorted(list(set(raw_text_no_spec_char)))
char_to_int = dict((c, i) for i, c in enumerate(chars))


# prepare the dataset of input to output pairs encoded as integers
seq_length = 100
dataX = []
dataY = []

for i in range(0, n_chars - seq_length, 1):
    seq_in = raw_text_no_spec_char[i:i + seq_length]
    seq_out = raw_text_no_spec_char[i + seq_length]
    dataX.append([char_to_int[char] for char in seq_in])
    dataY.append(char_to_int[seq_out])
n_patterns = len(dataX)
print("Total Patterns: ", n_patterns)

# reshape X to be [samples, time steps, features]
X = numpy.reshape(dataX, (n_patterns, seq_length, 1))

# normalize
X = X / float(n_vocab)

# one hot encode the output variable
y = np_utils.to_categorical(dataY)

# define the LSTM model
model = Sequential()
model.add(LSTM(256, input_shape=(X.shape[1], X.shape[2]), return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(256))
model.add(Dropout(0.2))
model.add(Dense(y.shape[1], activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam')

# load the network weights
filename = "weights/weights-improvement-35-1.2954_AiW2_256LSTM.hdf5"
model.load_weights(filename)
model.compile(loss='categorical_crossentropy', optimizer='adam')

# pick a random seed
start = numpy.random.randint(0, len(dataX)-1)
pattern = dataX[start]
print("Seed:")
print("\"", ''.join([int_to_char[value] for value in pattern]), "\"")

# generate characters
for i in range(1000):
	x = numpy.reshape(pattern, (1, len(pattern), 1))
	x = x / float(n_vocab)
	prediction = model.predict(x, verbose=0)
	index = numpy.argmax(prediction)
	result = int_to_char[index]
	seq_in = [int_to_char[value] for value in pattern]
	sys.stdout.write(result)
	pattern.append(index)
	pattern = pattern[1:len(pattern)]
print("\nDone.")